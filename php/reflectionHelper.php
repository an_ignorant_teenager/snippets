<?php
/**
 * reflectionHelper
 */
class reflectionHelper
{
    /**
     * getClassDoc 获取对象方法说明
     *
     * <code>
     * (new reflectionHelper())->getClassDoc(new \mysqli());
     * </code>
     * 
     * @param  object $object
     * @return void
     */
    public function getClassDoc($object)
    {
        $reflect = new \ReflectionClass($object);
        echo "<pre>Class: {$reflect->getName()}\r\n";
        echo str_pad("\r\n\r\n", 150, '=', STR_PAD_LEFT);

        $methods = $reflect->getMethods(\ReflectionMethod::IS_PUBLIC);
        foreach($methods as $method)
        {

            $param    = rtrim(implode($method->getParameters(), ','), ',');
            $modifier = implode(\Reflection::getModifierNames($method->getModifiers()), ' ');
            $comment  = $method->getDocComment();
            if(!empty($comment))
            {
                echo "{$comment}\r\n";
            }
            echo "{$modifier} {$method->name}({$param})\r\n\r\n";
        }
        echo str_pad("</pre>\r\n\r\n", 150, '=', STR_PAD_LEFT);
        die;
    }
}