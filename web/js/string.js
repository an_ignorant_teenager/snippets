/**
 * 首字母大写
 *
 * <code>
 * var str = 'terse';
 * console.log(str.ucfirst());
 * </code>
 * 
 * @return {string}
 */
String.prototype.ucfirst = function () {
    return this.slice(0, 1).toUpperCase() + this.slice(1);
};

/**
 * 单词首字母大写
 *
 * <code>
 * var str = "terse test";
 * console.log(str.ucwords());
 * </code>
 * 
 * @param  {string} separator
 * @return {string}
 */
String.prototype.ucwords = function (separator) {
    var words = this.split(' ');
    return words.map(function (word) {
        return word.ucfirst();
    }).join(' ');
};

/**
 * 字符分割形式转驼峰形式
 *
 * <code>
 * var str = "terse_test";
 * console.log(str.camelize());
 * console.log(str.camelize().ucfirst());
 * </code>
 * 
 * @param  {string} separator
 * @return {string}
 */
String.prototype.camelize = function (separator) {
    separator = typeof separator == 'undefined' ? '_' : separator;
    var lowerStr = this.toLowerCase();
    var words = lowerStr.split(separator);
    return words[0] + words.slice(1).map(function (word) {
        return word.ucfirst();
    }).join('');
};

/**
 * 驼峰形式转字符分割形式
 *
 * <code>
 * var str = "terseTest";
 * console.log(str.uncamelize());
 * </code>
 * 
 * @param  {string} separator
 * @return {string}
 */
String.prototype.uncamelize = function (separator) {
    separator = typeof separator == 'undefined' ? '_' : separator
    var word = this.replace(/([a-z])([A-Z])/, '$1' + separator + '$2')
    return word.toLowerCase()
}