# 项目简介

主要记录一些项目和学习当中用到的一些常用代码片段，仅供参考。

# 目录

## PHP

```
├── [身份证校验类](php/IDCardFilter.php)
│
├── [手机号操作类](php/mobileHelper.php)
│   *检测手机号是否正确、手机号中间四位隐藏*
│
├── [日期操作类](php/dateHelper.php)
│   *格式化为几[年|月|天|小时|分钟|秒]前等字符; 计算生日*
│
├── [字符串操作类](php/stringHelper.php)
│   *字符连接格式和驼峰格式互转*
│
├── [反射类](php/reflectionHelper.php)
│   *获取类的方法列表*
│
├── [验证码类](php/captchaHelper.php)
│   *生成指定长度的验证码，可以输出指定宽高的PNG图片*
│
├── [UA类](php/UAHelper.php)
│   *判断是否微信浏览器、安卓、IOS、手机*
│
├── [Redis并发锁类](php/redisHelper.php)
│   *如题*
│
└── [HttpClient操作类](php/HttpClientHelper.php)
    *实现get和post*
```

## web前端

```
├── MIXED
│   *一些实用的小实例*
│   ├── [微信分享demo](web/mixed/wx-share.html)
│   └── [CSS3 Loading](web/mixed/css3-loading.html)
│
├── CSS
│   *检测手机号是否正确、手机号中间四位隐藏*
│   ├── [引用样式](web/css/blockquote.md)
│   ├── [CSS首字母大写](web/css/ucfirst.css)
│   └── [CSS溢出省略](web/css/ellipsis.css)
│
└── JS
    └── [字符串](web/js/string.js)
        *首字母大写|单词首字母大写|字符连接格式和驼峰格式互转*
```

## sublime

```
├── [console.log](sublime/consolelog.sublime-snippet)
│   *js console.log*
│
└── [vardump.log](sublime/vardump.sublime-snippet)
    *php var_dump*
```